# coding=utf8
from influxdb import DataFrameClient

class ClientReadOnly(object):

    def __init__(self,host,port,username,password,db_name):
        """
        :param host:      influxdb host
        :param port:      influxdb port
        :param username:  influxdb username
        :param password:  influxdb password
        :param db_name:    influxdb dbname
        """
        self.client = DataFrameClient(host=host,
                            port=port, username=username,
                            password=password,
                            database=db_name)

    def get_ts_data(self,measurements,start="",end="",columns="*"):

        """
        :param measurements:
        :param start:
        :param end:
        :param columns: columns needed
        :return: df list
        """

        columnstr = ",".join(columns)
        measurementstr = ",".join(measurements)

        df_list = self.client.query('select {} from {} where time'.format(columnstr,measurementstr))

        return df_list
