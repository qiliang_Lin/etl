# coding=utf8
from influxdb import DataFrameClient
import logging
import json

def init_log():
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    logfile = './mv_log_0320.txt'
    fh = logging.FileHandler(logfile, mode='w+')
    fh.setLevel(logging.DEBUG)

    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    formatter = logging.Formatter("%(asctime)s %(levelname)s: %(message)s")
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)

    logger.addHandler(fh)
    logger.addHandler(ch)

    logger.info('Start log ...')


def get_latest_time(client, influxkey):
    A = client.query('select * from %s order by time desc limit 1' % influxkey)
    try:
        last_time_str = A[influxkey].index[0]
        last_time_str = last_time_str.strftime("%Y-%m-%d %H:%M:%S")
        logger.info(last_time_str)
    except Exception as e:
        logger.info('new meter,%s' % e)
        last_time_str = "2016-01-01T00:00:00Z"
    return last_time_str


def change(x):
    try:
        return round(float(x), 2)
    except:
        return x


def get_all_measurement(c_from):
    """
    :param c_from: influxdb DataFrameClient
    :return:
    """
    mlist = []

    for i in c_from.query('show measurements')['measurements']:
        mlist.append(i['name'])

    return mlist


def change_measurements(c_from, c_to):
    """
    :param c_from: source database type: influxdb.DataFrameClient connection
    :param c_to:   target database type: influxdb.DataFrameClient connection
    :return:       success,fail
    """

    measurements = get_all_measurement(c_from)

    f = open('/home/qiliang/influxlist.json','r')
    tb_dict = json.load(f)

    for tb_old,tb_new in tb_dict.items():

        if tb_old not in measurements:
            logger.info("%s not in influxdb" % tb_old)
            continue

        try:
            timestr = get_latest_time(c_to, tb_new)
        except Exception as e:
            logger.error(e)
            return False

        logger.info("%s %s" % (tb_old, tb_new))

        try:
            r = c_from.query("select * from %s where time>'%s' limit 9000" % (tb_old, timestr))
            logger.info("select * from %s where time>'%s' limit 9000" % (tb_old, timestr))
        except Exception as e:
            logger.error(e)
            return False

        if tb_old not in r.keys():
            logger.info("%s has no data" % tb_old)
            continue

        if len(r[tb_old]) == 0:
            continue

        ts = r[tb_old]
        ts = ts.applymap(change)
        ts = ts.where(ts.notnull(), None)
        c_to.write_points(ts, tb_new, protocol="json")

    return True


if __name__ == '__main__':

    init_log()

    c_from = DataFrameClient(host="120.132.7.37", port=8086, username="svcuser", password="svcuser",
                             database="EnergydB00")
    c_to = DataFrameClient(host="10.23.226.75", port=8086, username="equota", password="equota", database="db04")

    logger = logging.getLogger()

    change_measurements(c_from, c_to)
