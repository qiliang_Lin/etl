# coding=utf8
from influxdb import DataFrameClient
import json
import logging


def init_log():
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    logfile = '/home/qiliang/mv_2.0db_3.0db/mv_log_0321.txt'
    fh = logging.FileHandler(logfile, mode='w+')
    fh.setLevel(logging.DEBUG)

    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    formatter = logging.Formatter("%(asctime)s %(levelname)s: %(message)s")
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)

    logger.addHandler(fh)
    logger.addHandler(ch)

    logger.info('Start log ...')


def get_latest_time(client, influxkey):
    A = client.query('select * from %s order by time desc limit 1' % influxkey)
    try:
        last_time_str = A[influxkey].index[0]
        last_time_str = last_time_str.strftime("%Y-%m-%d %H:%M:%S")
        logger.info(last_time_str)

    except Exception as e:
        logger.info('new meter,%s' % e)
        last_time_str = "2016-01-01T00:00:00Z"
    return last_time_str


def change(x):
    try:
        return round(float(x), 2)
    except:
        return x


def change_columns(df, unit_type, unit_type_columns):
    return df.rename(columns=unit_type_columns[unit_type])


def copy_measurements(c_from,c_to):
    with open('/home/qiliang/mv_2.0db_3.0db/unit_type_col.json', 'r') as fp:
        unit_type_columns = json.load(fp)

    with open('/home/qiliang/mv_2.0db_3.0db/unitype_dict.json', 'r') as fp:
        enu_unit_type = json.load(fp)

    with open('/home/qiliang/mv_2.0db_3.0db/influxlist.json', 'r') as f:
        tb_list = json.load(f)


    for tb_old, tb_new in tb_list.items():

        timestr = get_latest_time(c_to, tb_new)

        logger.info("%s %s" % (tb_old, tb_new))

        r = c_from.query("select * from %s where time>'%s' limit 9000" % (tb_old, timestr))

        logger.info("select * from %s where time>'%s' limit 9000" % (tb_old, timestr))

        if tb_old not in r.keys():
            logger.info("%s has no data" % tb_old)
            continue

        if len(r[tb_old]) == 0:
            continue

        ts = r[tb_old]

        ts = ts.applymap(change)

        unit_type = enu_unit_type[tb_new]

        print(ts.columns)
        ts = change_columns(ts, unit_type, unit_type_columns)
        try:
            ts = ts.where(ts.notnull(), None)
        except:
            ts = ts.reindex(ts.index.drop_duplicates())
            print(ts.head())
            continue
        print(ts.columns)
        c_to.write_points(ts, tb_new, protocol="json")


if __name__ == '__main__':

    init_log()

    logger = logging.getLogger()

    c_from = DataFrameClient(host="120.132.7.37", port=8086, username="svcuser", password="svcuser",
                database="EnergydB00")

    c_to = DataFrameClient(host="10.23.226.75", port=8086, username="equota", password="equota", database="db03")

    copy_measurements(c_from=c_from,c_to=c_to)
