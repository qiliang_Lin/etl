#coding=utf8
#py2.0
import pandas as pd
import pymssql
import requests
import json
import logging
from   influxdb import InfluxDBClient
from   copy     import deepcopy
import calendar
import MySQLdb
from   datetime import timedelta
import time

def init_log():

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    logfile = '/home/appuserlinux/log/wanda_e_0319.txt'
    fh = logging.FileHandler(logfile, mode='a+')
    fh.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    formatter = logging.Formatter("%(asctime)s %(levelname)s: %(message)s")
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)

    logger.addHandler(fh)
    logger.addHandler(ch)
    logger.info('Start log ...')



class DataServer():

    def __init__(self,E_excel,plaza_id):

        self.logger = logging.getLogger()
        self.E_excel        =  E_excel
        self.sqlserver_conn = pymssql.connect(host='40.125.175.178',
                                    user='NJJNWD',charset='utf8',
                                    password='equota2017',
                                    database='NJJNWDDB')
        self.mysql_conn     = MySQLdb.connect(user='root',passwd='Admin@123',
                                db="nypt",host='172.31.6.34',charset = 'utf8')

        self.client         = InfluxDBClient('172.31.6.37', 8086, 'svcuser',
                                     'svcuser','EnergydB01')
        self.client1 = InfluxDBClient(host="120.132.30.116", port=8086, username="svcuser", password="svcuser", database="wanda_2016")
        self.latest_del_id  = 0

        self.plaza_id       = plaza_id
        self.get_latest_id(self.mysql_conn,"t_plaza_meter_dataV2")
        self.influxkey_dict = {}
        self.influxkey_list = self.get_influxkey_list()
        self.login()

    def delete_old_data(self):
        cur       = self.mysql_conn.cursor()
        sql_delete= "delete  from t_plaza_meter_dataV2\
                     where c_id<=%s" % (self.latest_del_id)
        cur.execute(sql_delete)
        self.mysql_conn.commit()

    def get_current_data(self):
        sql =  "select * from t_plaza_meter_dataV2 where c_id<=%s" % (self.latest_del_id+1000000)
        self.logger.info(sql)
        self.dev_data_df_all = pd.read_sql(sql.encode('utf-8'), self.mysql_conn)
        self.logger.info("get data OK")

    def get_max_id(self,t_name):
        cur    = self.mysql_conn.cursor()
        sql    = "select max(c_id) from %s" % t_name
        cur.execute(sql)
        id_last= cur.fetchall()[0][0]
        return id_last

    def update_max_id(self,max_id,t_name):
        cur    = self.mysql_conn.cursor()
        cur.execute("update t_data_last_id set c_id=%s where t_name='%s'" % (max_id,t_name))
        self.mysql_conn.commit()


    def get_influxkey_list(self):

        ms = self.client.query('show measurements')
        ms_dict = [i for i in ms][0]
        return [a[u'name'] for a in ms_dict]

    def get_latest_id(self,cli,t_name):
        cur           = cli.cursor()
        cur.execute("select c_id from t_data_last_id where t_name='%s'" % t_name)
        last_id       = cur.fetchall()
        print last_id[0][0]
        self.latest_del_id = last_id[0][0]

    def login(self):
        url = "http://40.125.171.121/api-token-auth/"
        data = {"error": 0, "password": "equotaadmin",
                "username": "equotaadmin", "rememberName": 0}
        res = requests.post(url, data=json.dumps(data))
        self.header = {'User-Agent': 'Mozilla/5.0',
                'Authorization': 'Bearer %s' % res.json()["token"],
                'Content-Type': 'application/json',
                'Accept':'application/json'}

    def get_device_influxkey(self,search_items=""):
        url = "http://%s:%s/api/energyunit/?format=json&%s" % \
              ('40.125.171.121', '', search_items)
        data = requests.get(url, headers= self.header)
        data = data.json()
        res = []
        for item in data["objects"]:
            if item["type"] != 2:
                res.append(item)
        return res[0][u'influxKey']

    def get_e_device_dict(self):
        xl = pd.ExcelFile(self.E_excel)
        self.E_device = xl.parse(u'E')
        self.E_dict = self.E_device[[u'point',
                    u'name']].drop_duplicates().set_index(
                    u'name').to_dict()[u'point']

    def get_e_columns_dict(self):

        self.E_param_dict = pd.read_excel(\
             '/home/appuserlinux/WangdaDataServer/c_meter_param.xls')\
            [['c_meter_function_code', 'column']].set_index(\
            ['c_meter_function_code']).to_dict()['column']

    def load_influxkeyjson(self,fp):

        self.influxkey_dict = json.load(fp)

    def create_influxkey_json(self):
        for name, meter in [(k, self.E_dict[k]) \
                for k in sorted(self.E_dict.keys())]:
            try:
                influxkey = self.get_device_influxkey(
                search_items="name=%s" % name.replace('#', '%23').replace('&',
                            '%26').replace('/', '%2F').replace(' ','%20'))
            except:
                self.logger.info('%s  do not have influx key' % name)
                continue
            self.influxkey_dict[str(codemeter)] = influxkey

        with open('/home/appuserlinux/WangdaDataServer/%s.json' % self.plaza_id, 'w') as fp:
            json.dump(self.influxkey_dict,fp)

    def process_e_device_data(self, meter, name):
        try:
            influxkey = self.influxkey_dict[str(meter)]
        except:
            self.logger.info('%s  do not have influx key' % name)
            return
        if len(self.dev_data_df_all)==0:
            return
        self.logger.info("%s %s %s" % (name,meter,influxkey))
        dev_data_df        = self.dev_data_df_all[self.dev_data_df_all['c_plaza_id']=="%s" %  self.plaza_id]

        if len(dev_data_df)==0:
            print("no new data")
            return
        dev_data_df        = dev_data_df[dev_data_df['c_meter_id']== u"%s" % meter]
        if len(dev_data_df)==0:
           return
        group_data_by_time = dev_data_df.groupby(u'c_time')

        json_list = []
        d = {}

        for (time_col, data_df) in group_data_by_time:

            d["measurement"] = influxkey
            fields = {}
            for idx in data_df.index:

                if self.E_param_dict[data_df.ix[idx,\
                    u'c_meter_function_code']]=='E':
                    continue

                fields[self.E_param_dict[data_df.ix[idx,\
                    u'c_meter_function_code']]] = float(data_df.ix[idx, u'c_data'])

            d["time"] = calendar.timegm((time_col +\
                timedelta(hours=-8)).to_datetime().utctimetuple())

            d["fields"] = fields

            json_list.append(deepcopy(d))

        self.logger.info("%s,%s" % (name,len(json_list)))
        self.client.write_points(json_list, time_precision="s")
        self.client1.write_points(json_list, time_precision="s")


def main_e():

 #   init_log()
    E_excel_JN  = u'/home/appuserlinux/WangdaDataServer/万达江宁_E_1205.xlsx'
  #  logger      = logging.getLogger()
    data_server = DataServer(E_excel_JN,"1000626")

    max_id      = data_server.get_max_id("t_plaza_meter_dataV2")
    data_server.delete_old_data()
    data_server.get_e_columns_dict()
    data_server.get_e_device_dict()

    data_server.get_current_data()
    print "jn"
    try:
        fp      = open("/home/appuserlinux/WangdaDataServer/1000626.json",'r')
        data_server.load_influxkeyjson(fp)
        fp.close()
    except Exception,e:
        print "File is not found.",e
        data_server.create_influxkey_json()

    meterlist = set(data_server.dev_data_df_all[data_server.dev_data_df_all['c_plaza_id']=='1000626']['c_meter_id'].values)
    print meterlist
    for name, meter in [(k, data_server.E_dict[k]) for k in sorted(data_server.E_dict.keys())]:
 #       print meter
        if "%s" % meter not in meterlist:
            continue
        print "jn",meter
        data_server.process_e_device_data(meter, name)
###############################
    print "LG"
    E_excel_LG   = u'/home/appuserlinux/WangdaDataServer/万达萝岗_E_1205.xlsx'
#    logger       = logging.getLogger()
    data_server1 = DataServer(E_excel_LG,"1000904")
    data_server1.get_e_columns_dict()
    data_server1.get_e_device_dict()
 #   data_server1.get_current_data()
    data_server1.dev_data_df_all = data_server.dev_data_df_all
    meterlist = set(data_server.dev_data_df_all[data_server.dev_data_df_all['c_plaza_id']=='1000904']['c_meter_id'].values)
    try:
        fp1       = open("/home/appuserlinux/WangdaDataServer/1000904.json",'r')
        data_server1.load_influxkeyjson(fp1)
        fp1.close()
    except Exception,e:
        print "File is not found.",e
        #data_server1.create_influxkey_json()
    for name, meter in [(k, data_server1.E_dict[k]) for k in sorted(data_server1.E_dict.keys())]:
        if "%s" % meter not in meterlist:
            continue
        print "LG",meter
        data_server1.process_e_device_data(meter, name)
#######################
    data_server.update_max_id(min([(data_server.latest_del_id+1000000),max_id]),"t_plaza_meter_dataV2")

    #logger.info('End Data process')

if __name__ == '__main__':
    init_log()
    logger      = logging.getLogger()
    while(1):
        try:
            main_e()
        except:
            time.sleep(60)
