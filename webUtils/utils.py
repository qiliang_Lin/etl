#coding=utf-8
from __future__ import unicode_literals
from __future__ import division
from datetime import datetime

import pandas as pd
import calendar
import os
import time
import dateutil
import pytz
import json
import requests


class Util(object):
    tz = pytz.timezone("Asia/Shanghai")

    @staticmethod
    def dt_to_ts(dt):

        if not dt.tzinfo:
            dt = Util.tz.localize(dt)

        return calendar.timegm(dt.utctimetuple())

    def ts_to_dt(self, timestamp):

        return datetime.fromtimestamp(timestamp, self.tz)
    
    def str_to_ts(self, timeStr, timeZoneStr=" +8"):
        timeStr = timeStr + timeZoneStr
        parsedStr = dateutil.parser.parse(timeStr)
        return calendar.timegm(parsedStr.utctimetuple())

    def str_to_dt(self, timeStr):

        dt = self.str_to_ts(timeStr)

        return self.ts_to_dt(dt)

    @staticmethod
    def read_csv(fname):

        raw_df = pd.read_csv(fname)
        raw_df.columns = ["time"]+list(raw_df.columns)[1:]
        index  = pd.to_datetime(raw_df["time"], utc=True)
        raw_df.index   = index

        df = raw_df.drop(["time"], axis=1)
        df.index = df.index.tz_convert("Asia/Shanghai")

        return df

    @staticmethod
    def read_json(fname):
        with open(fname, "rb") as r:
            return json.load(r)

    @staticmethod
    def write_json(fname, data):
        with open(fname, 'wb') as w:
            json.dump(data, w)

    @staticmethod
    def save_data(influx_keys, client, start="'2016-12-31 16:00:00'"):

        for key in influx_keys:
            sql = "select * from %s where time > %s" % (key, start)
            df  = dict(client.query(sql))[key]
            fname = os.path.join("test", "%s.csv" % key)
            df.to_csv(fname)
            time.sleep(2)


class AppClient(object):

    def __init__(self, user="equotaadmin", pswd="equotaadmin", server="localhost:8000"):
        self.user    = user
        self.pswd    = pswd
        self.server  = server
        self.__set_token()

    def __set_token(self):
        url  = ''.join(["http://", self.server, "/api-token-auth/"])
        vals = {"username": self.user, "password": self.pswd}
        data = json.dumps(vals)
        self.token = json.loads(requests.post(url,data).content)['token']

    def __send_request_to_DB(self, url, data):
        add     = {'User-Agent': 'Mozilla/5.0', 'Authorization': "Bearer "+self.token}
        request = urllib.request.Request(url, data, add)
        result  = urllib.request.urlopen(request, timeout=10)
        data    = result.read()
        info    = result.info()
        if 'http_authorization' in info.keys():
            self.token = info['HTTP_AUTHORIZATION'].split(' ')[-1]
        return data

    def get_series(self, ID, start, end, interval, operation):

        values = {"operation": operation}
        values["start_utc"] = Util.dt_to_ts(start)*1000
        values["end_utc"]   = Util.dt_to_ts(end)*1000
        values["interval"]  = interval

        url  = ''.join(["http://", self.server, "/api/get_series/", ID, '/'])

        result = self.__get_result(url, values)
        if result == []: return pd.DataFrame()

        df = pd.DataFrame(result)
        df['time'] = df.index.values
        df.index = pd.to_datetime(df.time, unit="ms", utc=True,infer_datetime_format=True)
        df.index = df.index.tz_convert("Asia/Shanghai")

        return df

    def __get_result(self, url, data):
        headers = {'User-Agent': 'Mozilla/5.0', 'Authorization': "Bearer "+self.token}
        r = requests.post(url, data=data, headers=headers)
        print(r.text)
        return r.json()

def login():
    url = "http://40.125.171.121/api-token-auth/"
    data = {"error": 0, "password": "equotaadmin",
            "username": "equotaadmin", "rememberName": 0}
    res = requests.post(url, data=json.dumps(data))
    header = {'User-Agent': 'Mozilla/5.0',
            'Authorization': 'Bearer %s' % res.json()["token"],
            'Content-Type': 'application/json',
            'Accept':'application/json'}
    return header
    
def get_device_influxkey(header,search_items=""):
    url = "http://%s:%s/api/energyunit/?format=json&%s" % \
          ('40.125.171.121', '', search_items)
    data = requests.get(url, headers= header)
    data = data.json()
    res = []
    for item in data["objects"]:
        if item["type"] != 2:
            res.append(item)
    return res

def get_appliance(header,search_items=""):
    url = "http://%s:%s/api/appliance/?format=json&%s" % \
          ('40.125.171.121', '', search_items)
    data = requests.get(url, headers= header)
    data = data.json()
    res = []
    for item in data["objects"]:
        res.append(item)
    return res
