# coding=utf-8
from __future__ import unicode_literals
from __future__ import division
from scipy.signal import savgol_filter
from datetime import datetime, timedelta

import pandas as pd
import calendar
import os
import time
import dateutil
import influxdb
import pytz
import json
import requests
from requests.auth import HTTPDigestAuth

import urllib
import urllib2


class Util(object):
    tz = pytz.timezone("Asia/Shanghai")

    @staticmethod
    def dt_to_ts(dt):

        if not dt.tzinfo:
            dt = Util.tz.localize(dt)

        return calendar.timegm(dt.utctimetuple())

    def ts_to_dt(self, timestamp):

        return datetime.fromtimestamp(timestamp, self.tz)

    def str_to_ts(self, timeStr, timeZoneStr=" +8"):
        # set timezone
        timeStr = timeStr + timeZoneStr
        # convert datetime format
        parsedStr = dateutil.parser.parse(timeStr)
        # return utc timestamp
        return calendar.timegm(parsedStr.utctimetuple())

    def str_to_dt(self, timeStr):

        dt = self.str_to_ts(timeStr)

        return self.ts_to_dt(dt)

    @staticmethod
    def read_csv(fname):

        raw_df = pd.read_csv(fname)
        raw_df.columns = ["time"] + list(raw_df.columns)[1:]
        index = pd.to_datetime(raw_df["time"], utc=True)
        raw_df.index = index

        df = raw_df.drop(["time"], axis=1)
        df.index = df.index.tz_convert("Asia/Shanghai")

        return df

    @staticmethod
    def read_json(fname):
        with open(fname, "rb") as r:
            return json.load(r)

    @staticmethod
    def write_json(fname, data):
        with open(fname, 'wb') as w:
            json.dump(data, w)

    @staticmethod
    def save_data(influx_keys, client, start="'2016-12-31 16:00:00'"):

        for key in influx_keys:
            sql = "select * from %s where time > %s" % (key, start)
            df = dict(client.query(sql))[key]
            sql = "select * from %s order by time desc limit 1" % key
            fname = os.path.join("test", "%s.csv" % key)
            df.to_csv(fname)
            time.sleep(2)


class InfluxClient(influxdb.InfluxDBClient):
    def __init__(self, host, username, password, database):
        super(InfluxClient, self).__init__(host, 8086, username, password, database)


class InfluxDFClient(influxdb.DataFrameClient):
    def __init__(self, host, username, password, database):
        super(InfluxDFClient, self).__init__(host, 8086, username, password, database)


class OutlierFilter(object):
    def __init__(self):
        pass

    def zscore(self, raw_sr, thresh=2.5, window_length=5, polyorder=3):
        np_fil = savgol_filter(raw_sr, window_length, polyorder, mode="nearest")
        sr_fil = pd.Series(np_fil, index=raw_sr.index, name=raw_sr.name)
        sr_res = raw_sr - sr_fil
        sr_sco = (sr_res - sr_res.mean()) / sr_res.std()
        index = sr_sco.loc[abs(sr_sco) < thresh].index

        return raw_sr.reindex(index).reindex(raw_sr.index)


class AppClient(object):
    def __init__(self, user="equotaadmin", pswd="equotaadmin", server="localhost:8000"):
        self.user = user
        self.pswd = pswd
        self.server = server
        self.__set_token()

    def __set_token(self):
        url = ''.join(["http://", self.server, "/api-token-auth/"])
        vals = {"username": self.user, "password": self.pswd}
        data = json.dumps(vals)
        add = {'User-Agent': 'Mozilla/5.0'}
        req = urllib2.Request(url, data, add)
        self.token = json.loads(urllib2.urlopen(req).read())['token']

    def __send_request_to_DB(self, url, data):
        add = {'User-Agent': 'Mozilla/5.0', 'Authorization': "Bearer " + self.token}
        request = urllib2.Request(url, data, add)
        result = urllib2.urlopen(request, timeout=10)
        data = result.read()
        info = result.info()
        if 'http_authorization' in info.keys():
            self.token = info['HTTP_AUTHORIZATION'].split(' ')[-1]
        return data

    def get_series(self, ID, start, end, interval, operation):

        values = {"operation": operation}
        values["start_utc"] = Util.dt_to_ts(start) * 1000
        values["end_utc"] = Util.dt_to_ts(end) * 1000
        values["interval"] = interval

        # data = urllib.urlencode(values)
        # print data
        # data = json.dumps(values)
        url = ''.join(["http://", self.server, "/api/get_series/", ID, '/'])

        #       print url
        # result = json.loads(self.__send_request_to_DB(url, data))
        result = self.__get_result(url, values)
        # print result
        if result == []: return pd.DataFrame()

        df = pd.DataFrame(result)
        # print df.columns
        df['time'] = df.index.values
        # print df['time']
        # print df
        df.index = pd.to_datetime(df.time, unit="ms", utc=True, infer_datetime_format=True)
        # print df.index
        # print datetime.strptime(df.index,"%Y%m%d %H:%M:%S")
        # df = df.set_index("time")
        df.index = df.index.tz_convert("Asia/Shanghai")

        return df

    def __get_result(self, url, data):
        headers = {'User-Agent': 'Mozilla/5.0', 'Authorization': "Bearer " + self.token}
        r = requests.post(url, data=data, headers=headers)
        # print(url)
        # print(data)
        # print(headers)
        # print(r)
        return r.json()


def login():
    url = "http://40.125.171.121/api-token-auth/"
    data = {"error": 0, "password": "equotaadmin",
            "username": "equotaadmin", "rememberName": 0}
    res = requests.post(url, data=json.dumps(data))
    header = {'User-Agent': 'Mozilla/5.0',
              'Authorization': 'Bearer %s' % res.json()["token"],
              'Content-Type': 'application/json',
              'Accept': 'application/json'}
    return header


def get_device_influxkey(header, search_items=""):
    url = "http://%s:%s/api/energyunit/?format=json&%s" % \
          ('40.125.171.121', '', search_items)
    data = requests.get(url, headers=header)
    data = data.json()
    res = []
    for item in data["objects"]:
        if item["type"] != 2:
            res.append(item)
    return res


def get_appliance(header, search_items=""):
    url = "http://%s:%s/api/appliance/?format=json&%s" % \
          ('40.125.171.121', '', search_items)
    data = requests.get(url, headers=header)
    data = data.json()
    res = []
    for item in data["objects"]:
        res.append(item)
    return res