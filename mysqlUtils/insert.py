#coding=utf8
import copy
import numpy as np
import pandas as pd
import pymysql


def insert_data(conn, table, df):
    """
    :param conn:  pymysql connection
    :param table: table
    :param df: the DataFrame has the same columns like the tables
    :return: True,False
    """
    cur = conn.cursor()

    for idx in df.index:

        valst = []
        colst = []

        for col in df.columns.values:
            if not df.isna().ix[idx, col]:
                colst.append("`%s`" % col)
                valst.append("'%s'" % df.ix[idx, col])

        colstr = '(' + ','.join(colst) + ')'
        valuestr = '(' + ','.join(valst) + ')'

        sql = "insert into %s %s values %s" % (table, colstr, valuestr)
        cur.execute(sql)
        conn.commit()
    return True

def get_data(conn,table):
    """

    :param conn: mysql connection - pymysql connection
    :param table:  table we want to read -str
    :return: the data in Dataframe  -DataFrame
    """
    return pd.read_sql("select * from %s" % table, conn)
