#coding=utf8
import json
import copy
import numpy as np
import pandas as pd
import pymysql
import influxdb
import difflib
mysql_from      = pymysql.connect("10.23.102.144","root","Equotaenergy12#$","maindb8",charset="utf8")
cur_from        = mysql_from.cursor()
#mysql_to      = pymysql.connect("localhost","root","root","maindb11",charset="utf8")
mysql_to      = pymysql.connect("10.23.102.144","root","Equotaenergy12#$","app_v3",charset="utf8")
cur_to          = mysql_to.cursor()

def insert_data(conn,table,df):
    cur = conn.cursor()
    colstr  = '('+','.join(["`%s`" % col for col in df.columns.values])+')'
         
    for idx in df.index:
        valuestr= '('+','.join(["'%s'" % df.ix[idx,col] for col in df.columns.values])+')'
        sql = "insert into %s %s values %s" % (table,colstr,valuestr)
        print(sql)
        cur.execute(sql.encode('utf8'))
        conn.commit()
    return pd.read_sql("select * from %s" % table,conn)

def get_data(conn,table):
    return pd.read_sql("select * from %s" % table,conn)

# add alerts catogrys
sql = "select * from alert_alertcategory"
df_alert_alertcategory = pd.read_sql(sql,mysql_from)
alcatdf = df_alert_alertcategory.rename(columns={"display_name":"description"})
#insert_data(mysql_to,'alert_alertcategory',alcatdf)

df_alert_cat_new = get_data(mysql_to,'alert_alertcategory')

act = pd.merge(right=df_alert_cat_new,left=df_alert_alertcategory,left_on='name',right_on='name')

map_act_dict = {}
for old,new in zip(act.id_x.values,act.id_y.values):
    map_act_dict[old] = new


#add alerts typs
sql     = "select * from alert_alerttype"
df_type = pd.read_sql(sql.encode('utf-8'),mysql_from)
df_type['name'] = ['indoor_comfort','ahu_rule_based_apar_rules',
                   'vavbox_rule_based_apar_rules','elec_quality_rule_based']
alert_ty_df = df_type[['name','classname','filename','alert_category_id']].rename(columns={'classname':'file_name','filename':'folder_name'})
alert_ty_df['is_active'] =1
#insert_data(mysql_to,'alert_alerttype',alert_ty_df)
alert_type_map = {17:1,18:2,19:3,24:4}

#alert prioritys
sql     = "select * from alert_alertpriority"
df_prioritys = pd.read_sql(sql.encode('utf-8'),mysql_from)
df_prioritys = df_prioritys.rename(columns={'display_name':'description'})
#insert_data(mysql_to,'alert_alertpriority',df_prioritys)

#alert status

sql     = "select * from alert_alertstatus"
df_status = pd.read_sql(sql.encode('utf-8'),mysql_from)
df_status = df_status.rename(columns={"display_name":"description"})
#insert_data(mysql_to,'alert_alertstatus',df_status)

#alert task
sql     = "select * from alert_alert limit 10"
df_alerttask = pd.read_sql(sql.encode('utf-8'),mysql_from)
df_alerttask = df_alerttask[['alertconfig_json',
                             'is_active',
                             'alert_type_id',
                             'last_notification_time'
                            ]].rename(columns={'alertconfig_json':'config',
                                              'last_notification_time':'alert_ctl_time'})
def renew(df):
    return alert_type_map[df['alert_type_id']]
df_alerttask['alert_type_id'] = df_alerttask.apply(renew,axis=1)
df_alerttask = df_alerttask.loc[0:3]
df_alerttask['building_id'] = 1
#print(df_alerttask)
#insert_data(mysql_to,'alert_alerttask',df_alerttask)

aler_task_map = {32:1,33:2,34:3,35:4}

#alerts logs
app_df = pd.read_sql('select * from common_appliance',mysql_to)
app_dict = dict(zip(app_df.name.values,app_df.id.values))

sql     = "select * from alert_alertlog left join backend_energyunit on alert_alertlog.id=backend_energyunit.id"
df_alertlog = pd.read_sql(sql.encode('utf-8'),mysql_from)

df_alertlog1 = df_alertlog[['accept_time',
                             'id',
                             'alert_id',
                             'alert_time',
                             'analysis',
                             'confirm_time',
                             'desc',
                             'energy_saved',
                             'equipment_loss',
                             'ignore_time',
                             'is_checked',
                             'last_report_time',
                             'operate_time',
                             'operate_user_id',
                             'title',
                             'user_id',
                             'priority',
                             'highchart_plot',
                             'uncomfort',
                             'unsecurity',
                             'name',
                             'alertstatus_id']].rename(columns={'priority':'priority_id',
                                                                   'highchart_plot':'chart',
                                                                   'alertstatus_id':'alert_status_id',
                                                                   'alert_id':'alert_task_id',
                                                                   'uncomfort':'uncomfortable',
                                                                   'unsecurity':'insecurity',
                                                                   'energy_saved':'cost_saved',
                                                                   'desc':'description'
                                                                   })
aler_task_map[36] = 4

df_alertlog1  = df_alertlog1.dropna(axis=1)

def renew_alert(df):
    return aler_task_map[df['alert_task_id']]

df_alertlog1['alert_task_id'] = df_alertlog1.apply(renew_alert,axis=1)
df_alertlog1['cost_saved']   = 0
#df_alertlog1['appliance_id'] = 5686
df_alertlog1['energy_saved'] = 0
df_alertlog1['accept_time']  = '2018-03-09 00:00:00'
df_alertlog1['confirm_time'] = '2018-03-09 00:00:00'
df_alertlog1['ignore_time']  = '2018-03-09 00:00:00'
df_alertlog1['operate_time']  = '2018-03-09 00:00:00'
df_alertlog1['user_id']  = 2
#筛选条件1 按时间段筛选
#df_select = df_alertlog1[df_alertlog1['last_report_time']>'2017-10-01'][df_alertlog['last_report_time']<'2017-11-01']
#筛选条件2 按照id 筛选
df_select = df_alertlog1.set_index('id')


id_list =  [56696,6623,6624,58991, 59002, 59017, 59036,
         59039, 59053,58927, 58992,6671,6675,7265,
         7391,6674, 6678, 7268, 7394,
        62444,62472,62495,62520,62561,62570,
         62635,62652,62653,62683,62686,
         62700,62709,62711,62712,62733,64749,64664,64809,64803,64793]

df_select = df_select.reindex(id_list).dropna()

def update_chart(df):
    return str(json.loads(df['chart'])).replace("'","\"")

def update_description(df):
    desc = {}
    desc['description'] = json.loads(df['analysis'])
    return str(desc).replace("'","\"")

def update_analysis(df):
    return str(json.loads(df['description'])['description']).replace("'","\"")


app_name_list = []
for name in df_select.title.values:
    #print(name)
    max_ratio = 0
    for key in app_dict.keys():
        ratio = difflib.SequenceMatcher(None, key, name).ratio()
        if ratio>max_ratio:
            max_ratio = ratio
            app = key
    app_name_list.append(app_dict[app])
df_select['appliance_id'] = app_name_list

df_select2 = copy.deepcopy(df_select)

df_select2['analysis']=df_select.apply(update_analysis,axis=1)
df_select2['description']=df_select.apply(update_description,axis=1)
df_select2['chart']=df_select.apply(update_chart,axis=1)

df_select2.to_excel('d.xls')
df_select2 = pd.read_excel('d.xls')
del df_select2['id']

#insert_data(mysql_to,'alert_alertlog',df_select2)
