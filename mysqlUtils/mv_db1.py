#coding=utf8
import copy
import numpy as np
import pandas as pd
import pymysql
import influxdb

mysql_to      = pymysql.connect("localhost","root","root","maindb10",charset="utf8")
cur_to        = mysql_to.cursor()
mysql_from    = pymysql.connect("localhost","root","root","maindb9",charset="utf8")
cur_from      = mysql_from.cursor()

def insert_data(conn,table,df):
    cur = conn.cursor()
    colstr  = '('+','.join(["`%s`" % col for col in df.columns.values])+')'
    
    for idx in df.index:
        valuestr= '('+','.join(["'%s'" % df.ix[idx,col] for col in df.columns.values])+')'
        sql = "insert into %s %s values %s" % (table,colstr,valuestr)
        #print(sql)
        cur.execute(sql)
        conn.commit()
    return pd.read_sql("select * from %s" % table,conn)

def get_data(conn,table):
    return pd.read_sql("select * from %s" % table,conn)


city_old = get_data(mysql_from,'common_city')
city_df = city_old
city_df = city_df[['name','gps_location','description']]

#insert_data(mysql_to,'common_city',city_df)

city_r = get_data(mysql_to,'common_city')



com_old = get_data(mysql_from,'common_company')

com_df = com_old[['name','address','email','phone','description']]
#insert_data(mysql_to,'common_company',com_df)


company_r = get_data(mysql_from,'common_company')


loca_df = get_data(mysql_from,'common_location')

del loca_df['id']

#insert_data(mysql_to,'common_location',loca_df)


weather_r = get_data(mysql_from,'common_weather')
del weather_r['id']
#insert_data(mysql_to,'common_weather',weather_r)

cp_r = get_data(mysql_from,'common_campusparam')
del cp_r['id']
#insert_data(mysql_to,'common_campusparam',cp_r)

campus_r = get_data(mysql_from,'common_campus')
del campus_r['id']
#insert_data(mysql_to,'common_campus',campus_r)


buildingtype = get_data(mysql_from,'common_buildingtype')

del buildingtype['id']
#insert_data(mysql_to,'common_buildingtype',buildingtype)

buildingparam = get_data(mysql_from,'common_buildingparam')
print(buildingparam)
del buildingparam['id']
#insert_data(mysql_to,'common_buildingparam',buildingparam)

building = get_data(mysql_from,'common_building')
print(building)
del building['id']
#insert_data(mysql_to,'common_building',building)


systemtype_old = get_data(mysql_from,'common_systemtype')

del systemtype_old['id']
#systemtype_new = insert_data(mysql_to,'common_systemtype',systemtype_old)

system_df = get_data(mysql_from,'common_system')
#首先插入没有父节点的system
fs = system_df[system_df.parent_id.isna()][['name','building_id','system_type_id']]
#insert_data(mysql_to,'common_system',fs)

ss = system_df[system_df.parent_id.notna()]


def insert_data_one(conn,table,df):
    cur = conn.cursor()
    colstr  = '('+','.join(["`%s`" % col for col in df.columns.values])+')'
    
    for idx in df.index:
        valuestr= '('+','.join(["'%s'" % df.ix[idx,col] for col in df.columns.values])+')'
        sql = "insert into %s %s values %s" % (table,colstr,valuestr)
        #print(sql)
        cur.execute(sql)
        conn.commit()
    return pd.read_sql("select max(id) from %s" % table,conn)



fs_new = get_data(mysql_to,'common_system')

name_id_dict_old = {}
for i_d,name in zip(system_df.id,system_df.name):
    name_id_dict_old[i_d] = name

name_id_dict_new = {}

for i_d,name in zip(fs_new.id,fs_new.name):
    name_id_dict_new[name] = i_d
print(name_id_dict_new)
temp_df = pd.DataFrame(columns=['name','building_id','parent_id','system_type_id'])
for idx in ss.index:
    print(name_id_dict_old[ss.ix[idx,'parent_id']])
    temp_df.loc[0] = [ss.ix[idx,'name'],ss.ix[idx,'building_id'],
                      name_id_dict_new[name_id_dict_old[ss.ix[idx,'parent_id']]],
                     ss.ix[idx,'system_type_id']]
#    df = insert_data_one(mysql_to,'common_system',temp_df)
#    print(df.ix[0,'max(id)'])

#    name_id_dict_new[ss.ix[idx,'name']] = df.ix[0,'max(id)']
    #print(name_id_dict_new)

sys_old_new = {}
for i_d in name_id_dict_old.keys():
    sys_old_new[i_d] = name_id_dict_new[name_id_dict_old[i_d]]


at_r = get_data(mysql_from,'common_appliancetype')

#applianctype_new = insert_data(mysql_to,'common_appliancetype',at_r)

df_seg = get_data(mysql_from,'common_segment')

#insert_data(mysql_to,'common_segment',df_seg)

applance = get_data(mysql_from,'common_appliance')

locateT = get_data(mysql_from,'common_locatetype')

#insert_data(mysql_to,'common_locatetype',locateT)
location_new = get_data(mysql_from,'common_location')
print(location_new)
#try:
#    insert_data(mysql_to,'common_location',location_new)
#except Exception as e:
#    print(e)
locate = get_data(mysql_from,'common_locate')

insert_data(mysql_to,'common_locate',locate[['id','name','area','building_id','locate_type_id']])

sys_old_new = {}
for i_d in name_id_dict_old.keys():
    sys_old_new[i_d] = name_id_dict_new[name_id_dict_old[i_d]]

def update_system_id(df):
    return sys_old_new[df['system_id']]
applance['system_id']=applance.apply(update_system_id,axis=1)


at_r = get_data(mysql_from,'common_appliancetype')
applianctype_new = get_data(mysql_to,'common_appliancetype')

ndf = pd.merge(right=at_r,left=applianctype_new,right_on='name',left_on='name')


df_subseg = get_data(mysql_from,'common_subsegment')
del df_subseg['id']
insert_data(mysql_to,'common_subsegment',df_subseg)

#print(ndf)
app_old_to_new = {}
for x,y in zip(ndf.id_x,ndf.id_y):
    app_old_to_new[y] = x

def get_id(df):
    return app_old_to_new[df['appliance_type_id']]
applance['appliance_type_id']=applance.apply(get_id,axis=1)

del applance['parent_id']
del applance['manufacturer_id']
applance['manufacturer_date'] = '2010-01-01 00:00:00'
insert_data(mysql_to,'common_appliance',applance)

unitype = get_data(mysql_from,'common_energyunittype')
print(unitype.columns)
insert_data(mysql_to,'common_energyunittype',unitype[['name','description','fields','default']])

enu_data = get_data(mysql_from,'common_energyunit')
print(enu_data.columns)
insert_data(mysql_to,'common_energyunit',enu_data[['name','energy_unit_type_id','measurement','fields']])

app_has_enu = get_data(mysql_from,'common_energyunit_appliances')
del app_has_enu['id']
insert_data(mysql_to,'common_energyunit_appliances',app_has_enu)

