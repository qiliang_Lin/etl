#coding=utf8
import pandas as pd
import pymssql
import requests
import json
import logging
from influxdb import InfluxDBClient
from copy import deepcopy
import calendar
from datetime import datetime
from datetime import timedelta
from copy import deepcopy
import numpy as np


def init_log():
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    logfile = './log/logger0314.txt'
    fh = logging.FileHandler(logfile, mode='w+')
    fh.setLevel(logging.DEBUG)

    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    formatter = logging.Formatter("%(asctime)s %(levelname)s: %(message)s")
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)

    logger.addHandler(fh)
    logger.addHandler(ch)


class DataServerLuogan():

    def __init__(self):
        self.logger = logging.getLogger()
        self.BA_excel = u'万达萝岗_BA_20171205.xlsx'
        self.conn = pymssql.connect(host='10.53.48.198',
                                    user='huiyun',charset='utf8',
                                    password='huiyun',
                                    database='V2_WDL2')

        self.client = InfluxDBClient('172.31.6.37', 8086, 'svcuser',
                                     'svcuser', 'EnergydB01')
        self.login()

    def login(self):
        A = self.client.query('select * from %s order by time limit 1' % influxkey)
        # print A
        # last_time_str = [X[0]['time'] for X in A][0]
        last_time_str = '2017-03-14T00:00:00Z'
        for X in A:
            try:
                last_time_str = X[0]['time']
            except Exception as e:
                print('new meter,%s' % e)
            break
        time = datetime.strptime(last_time_str, '%Y-%m-%dT%H:%M:%SZ') + timedelta(hours=8)
        last_time_str = time.strftime("%Y-%m-%d %H:%M:%S")
        return last_time_str, time

    def get_device_influxkey(self, search_items=""):
        url = "http://%s:%s/api/energyunit/?format=json&%s" % \
              ('40.125.171.121', '', search_items)
        data = requests.get(url, headers=self.header)
        data = data.json()
        res = []
        for item in data["objects"]:
            if item["type"] != 2:
                res.append(item)
        return res[0][u'influxKey']

    def get_ba_device_list(self):
        self.BA_device = pd.read_excel(self.BA_excel)
        self.BA_device.loc[self.BA_device.attribute.isnull(), 'attribute'] = self.BA_device.ix[
            self.BA_device.attribute.isnull(), 'name']
        self.BA_list = list(set(list(self.BA_device.ix[:, u'name'])))

    def process_device_data(self):

        luogan_ba_group = self.BA_device.groupby(u'name')

        for dev,df in luogan_ba_group:
            if dev not in [u'大商业总管参数室外']:
                continue
            self.logger.info(dev)
            df_list = []
            for idx in df.index:
                sql = "select * from %s#%s#span  where time>'2018-04-01 00:00:00' order by time desc" % (
                    df.ix[idx,u'point'],df.ix[idx,u'column_old'])
                try:
                    unit_data_df = pd.read_sql(sql,self.conn).rename(
                    columns={"value": df.ix[idx,u'column']}).set_index('time')
                except:
                    self.logger.error('%s, %s' % (sql, df.ix[idx, u'point']))
                if len(unit_data_df)==0:
                    continue
                df_list.append(unit_data_df)
            if len(df_list)==0:
                continue
            dict_dev = pd.concat(df_list, axis=1)
            self.logger.info(dict_dev.head())
            self.logger.info('querry sql over')
            dict_dev.to_excel('%s.xlsx' % dev)

def main():
    init_log()
    data_server = DataServerLuogan()
    data_server.get_ba_device_list()
    data_server.process_device_data()


if __name__ == '__main__':
    main()

